---
title: Welcome
---
Hi, I'm Marco Venuti. I am currently a PhD student at [SISSA](https://www.sissa.it/) in the [Theoretical Particle Physics](https://www.sissa.it/tpp/index.php) group.
Previously (from 2017 to 2022), I was a physics student at [SNS](https://www.sns.it/it/) and [UniPi](https://www.df.unipi.it/).

Here are some of my social profiles or ways to contact me
<p>
{% for item in site.data.contacts %}
<div><i class="{{ item.icon-class }}"></i> <a href="{{ item.href }}" {% if item.tooltip %}data-bs-toggle="tooltip" data-bs-placement="right" data-bs-title="{{ item.tooltip }}"{% endif %}>{{ item.text }}</a></div>
{% endfor %}
</p>
