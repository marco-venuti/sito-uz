---
title: Free time
---

During university, besides studying at [SNS](https://www.sns.it/it/), I have been a part-time collaborator with the School as a sysadmin for [UZ](https://uz.sns.it), the students' unified IT network. In particular in the first years I have mostly dedicated my time to administrating Starfleet, the IT structure of [Collegio Timpano](https://wiki.uz.sns.it/timpano/home), where I have lived for four years. In the last years I mostly worked in deeply renovating the UZ storage and service infrastructure, and in uniforming it with Starfleet.

I endorse free software, whenever possible. My machines (and also those of UZ and Starfleet) run mainly [Debian](https://www.debian.org/).
In the [Editor War](https://en.wikipedia.org/wiki/Editor_war), I take the side of [emacs](https://www.gnu.org/software/emacs/), which I use for everything. You can find my emacs configuration [here](https://gitlab.com/marco-venuti/emacs-config).
