---
title: University
---

Here are some "notes" that I (and others) have been writing during university and my PhD.
For further useful notes you can check out [Alessandro Piazza](https://uz.sns.it/~alepiazza/uni.html)'s and [Giuseppe Bogna](https://uz.sns.it/~BeppeBogna/uni.html)'s personal pages.

Please note that some of these works are not ready yet and thus not publicly available.

{% for section in site.data.uni %}
<h3>{{ section.title }}</h3>
<div class="row row-cols-1 row-cols-md-4 g-2">
  {% for item in section.contents %}
  <div class="col {% if item.muted %}text-muted{% endif %}">
    <div class="card h-100">
      <div class="card-header">{{ item.title }}</div>
      <div class="card-body">
        <p class="card-text">{{ item.description }}</p>
      </div>
      {% if item.muted != true %}
      <div class="card-footer">
        <div class="btn-group" role="group">
          {% if item.href-gitlab %}<a href="{{ item.href-gitlab }}" class="btn btn-secondary"><i class="fab fa-gitlab"></i></a>{% endif %}
          {% if item.href-web %}<a href="{{ item.href-web }}" class="btn btn-secondary"><i class="fas fa-link"></i></a>{% endif %}
          {% if item.href-down %}<a href="{{ item.href-down }}" class="btn btn-secondary" download><i class="fas fa-file-download"></i></a>{% endif %}
        </div>
      </div>
      {% endif %}
    </div>
  </div>
  {% endfor %}
</div>
<br />
{% endfor %}
