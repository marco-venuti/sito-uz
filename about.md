---
title: About
---
This website is made using [Jekyll](https://jekyllrb.com/), a static website generator, and is hosted on UZ's servers.
You can find the repository [here](https://gitlab.com/marco-venuti/sito-uz).
